# Lounge fm parser

There is public api http://radioclub.ua/json/playlist.json with info about songs that have been played resently.
Also json contains:

* id
* Song name
* Song title
* link to mp3 file
* link to cover image
* duration
* etc.

This script parses this json every 10 minutes and stores all info about song in database (SqlAlchemy ORM) `schedule_continious_parsing()`
(This particular script gets info about songs from https://www.loungefm.com.ua/terrace radio)

You need approximately one week to get 1500 songs

After that you need to clean up your loot:

* Delete all ununique enterties in database  `delete_ununique_items_in_db()`
(by design id is primary key. by fact id is just incremented so you can get one song several time)
Check title and author is a better idea. `delete_songs_with_empty_song_url_from_db()`
* Delete all enterties with empty song_url `delete_songs_with_empty_song_url_from_db()`
* Delete all enterties with empty image_url `delete_songs_with_empty_image_url_form_db()`

After that you can download:
* all songs `download_all_songs()`
* all covers `download_all_covers()`

And now you can add some magic by adding metadata to your downloaded songs
(image, title, author)
`add_metadata_to_song()`

Script downloaded and taged ~1440 songs (~97% from all parsed songs of lounge fm terrace)
This script can be used for downloading songs from other radio stations that avaliable on http://radioclub.ua

Script can bee run in docker. You have to mount database file in order to save data across starts
```
# Localy
docker build -t parser .
docker run -v ./db/songs_db.sqllite:/code/db/songs_db.sqllite parser

# Or you can run it from builded container from registry
docker login registry.gitlab.com
docker pull egistry.gitlab.com/kovalenko.bogdan/lounge-fm-parser:latest
docker run -v $(echo $PWD)/db:/code/db/ registry.gitlab.com/kovalenko.bogdan/lounge-fm-parser
```

All parsed songs from loungefm terrace stored in `parsed_songs_db.sqllite`

Good luck in hunt for music
