import os
from time import time as timer
from time import sleep
import urllib
import json
import requests
import schedule
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError
import eyed3


arguments = []

if not os.path.exists("db"):
    os.mkdir("db")

engine = create_engine('sqlite:///db/songs_db.sqllite')
Base = declarative_base()


class Track(Base):
    """Model for storing song data in database"""

    __tablename__ = 'tracks'

    id = Column(Integer, primary_key=True)
    artist = Column(String)
    name = Column(String)
    song_url = Column(String)
    image_url = Column(String)

    def __repr__(self):
        return "%s - %s" % (self.name, self.artist)


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


def get_songs():
    """Gets json with currently playing songs from public api and saves it in database"""
    with urllib.request.urlopen("http://radioclub.ua/json/playlist.json") as url:
        data = json.loads(url.read().decode())

    lounge_fm_playlist = []

    for playlist in data["list"]:
        if playlist["station"] == 'loungefm' and playlist["stream"] == 'terrace':
            lounge_fm_playlist = playlist['playlist']

    for item in lounge_fm_playlist:
        try:
            track = Track(id=item['id'], artist=item['artists'][0]['title'], name=item['track']['title'],
                          song_url=item['track']['sound'], image_url=item['artists'][0]['image'])
            session.add(track)
            session.commit()
        except IntegrityError:
            session.rollback()


def fetch_url(entry):
    """Downloads files. Takes tuple as argument. ('path to save', 'external url')"""
    path, uri = entry
    if not os.path.exists(path):
        r = requests.get(uri, stream=True)
        if r.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
    return path


def generate_tuples_with_songs_data():
    """Genetate list with tuples of path and song uri"""
    global arguments
    arguments = []

    for item in session.query(Track).all():
        print((item.name, item.song_url))
        arguments.append(("./songs/" + item.name + ' - ' + item.artist + '.mp3',
                          'http://radioclub.ua/' + item.song_url))


def generate_tuples_with_image_data():
    """Genetate list with tuple of path and image uri"""
    global arguments
    arguments = []

    for item in session.query(Track).all():
        print((item.name, item.song_url))
        arguments.append(("./images/" + item.name + ' - ' + item.artist + '.' + item.image_url.split(".")[-1:][0],
                          'http://radioclub.ua/' + item.image_url))


def download_all_songs():
    """Download all songs"""

    generate_tuples_with_songs_data()

    for entry in arguments:
        try:
            fetch_url(entry)
        except:
            print("Error with song " + entry[0])


def download_all_covers():
    """Download all covers"""

    generate_tuples_with_image_data()

    for entry in arguments:
        try:
            fetch_url(entry)
        except:
            print("Error with image " + entry[0])


def delete_ununique_items_in_db():
    """Clean ununique items from database"""
    for item in session.query(Track).all():
        a = session.query(Track).filter(Track.name == item.name, Track.artist == item.artist).all()
        for song in a[1:]:
            session.delete(song)
            session.commit()
        print(a)


def delete_songs_with_empty_song_url_from_db():
    """Clen items with null url"""
    for item in session.query(Track).filter(Track.song_url == None).all():
        print(item.name)
        session.delete(item)
        session.commit()


def delete_songs_with_empty_image_url_form_db():
    """Clen items with null image url"""
    for item in session.query(Track).filter(Track.image_url == None).all():
        print(item.name)
        session.delete(item)
        session.commit()


def add_metadata_to_song():
    """Adds song's title, artist and cover image to song's metadata"""
    for item in session.query(Track).all():
        try:
            audiofile = eyed3.load("./songs/" + item.name + ' - ' + item.artist + '.mp3')

            if audiofile.tag == None:
                audiofile.initTag()

            # File path with right file extention (".jpg" or ".png")
            file_path = "./images/" + item.name + ' - ' + item.artist + '.' + item.image_url.split(".")[-1:][0]

            audiofile.tag.images.set(3, open(file_path, 'rb').read(), 'image/jpeg')
            audiofile.tag.artist = item.artist
            audiofile.tag.title = item.name

            audiofile.tag.save()
        except:
            print(item.name + ' - ' + item.artist + '.mp3')


def schedule_continious_parsing():
    """Schedule continious parsing of public api"""
    get_songs()
    schedule.every(10).minutes.do(get_songs)

    while True:
        schedule.run_pending()
        sleep(1)


if __name__ == '__main__':

    # Get program start point
    start = timer()
    schedule_continious_parsing()
    # Print execution time
    print(f"Elapsed Time: {timer() - start}")
