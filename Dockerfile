FROM python:3.6-alpine
ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt
RUN apk add libmagic
CMD ["python", "main.py"]
